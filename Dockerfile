FROM openjdk:8-jdk-alpine
ARG JAR_FILE=build/libs/*jar
COPY ${JAR_FILE} app.jar
EXPOSE 8080
COPY wait-for-it.sh /wait-for-it.sh
RUN chmod +x /wait-for-it.sh
#apk command to install bash
RUN apk update && apk add bash
ENTRYPOINT ["./wait-for-it.sh","gramline-mysql:3306","--timeout=0","--strict", "--", "java","-jar","/app.jar"]