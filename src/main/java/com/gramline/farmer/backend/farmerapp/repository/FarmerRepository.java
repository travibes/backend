package com.gramline.farmer.backend.farmerapp.repository;

import com.gramline.farmer.backend.farmerapp.model.Farmer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Component
public interface FarmerRepository extends CrudRepository<Farmer, Integer> {
}
