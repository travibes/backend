package com.gramline.farmer.backend.farmerapp.service;

public enum ProductType {
    SEED, PESTICIDE;
}
