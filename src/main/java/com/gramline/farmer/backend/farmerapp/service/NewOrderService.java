package com.gramline.farmer.backend.farmerapp.service;

import com.gramline.farmer.backend.farmerapp.model.Order;
import com.gramline.farmer.backend.farmerapp.model.OrderItem;
import com.gramline.farmer.backend.farmerapp.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class NewOrderService {

    @Autowired
    private OrderRepository orderRepository;

    public void saveNewOrder(Order orderRequest) {
        for (OrderItem orderItem : orderRequest.getOrderItems()) {
            orderItem.setOrder(orderRequest);
        }
        orderRepository.save(orderRequest);
    }
}
