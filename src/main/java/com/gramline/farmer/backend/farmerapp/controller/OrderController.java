package com.gramline.farmer.backend.farmerapp.controller;

import com.gramline.farmer.backend.farmerapp.model.Order;
import com.gramline.farmer.backend.farmerapp.service.NewOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    @Autowired
    private NewOrderService newOrderService;

    @RequestMapping(value = "/saveneworder", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity saveOrder(@RequestBody Order orderRequest) {
        newOrderService.saveNewOrder(orderRequest);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
