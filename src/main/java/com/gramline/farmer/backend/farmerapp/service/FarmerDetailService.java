package com.gramline.farmer.backend.farmerapp.service;

import com.gramline.farmer.backend.farmerapp.model.Farmer;
import com.gramline.farmer.backend.farmerapp.repository.FarmerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FarmerDetailService {

    @Autowired
    private FarmerRepository farmerRepository;

    public Farmer savedetails(Farmer farmerPrimaryDetails) {
        return (farmerRepository.save(farmerPrimaryDetails));
    }
}
