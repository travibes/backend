package com.gramline.farmer.backend.farmerapp.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "`order`")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Column(nullable = false)
    private Double orderAmount;

    @CreationTimestamp
    private LocalDateTime orderDate;

    @Column(nullable = false, columnDefinition="tinyint(1)")
    private boolean amountPayed;

    @ManyToOne
    @JoinColumn
    private Farmer farmer;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "order")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<OrderItem> orderItems;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public boolean isAmountPayed() {
        return amountPayed;
    }

    public void setAmountPayed(boolean amountPayed) {
        this.amountPayed = amountPayed;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public String toString() {
        return "Order{" +
                "Id=" + Id +
                ", orderAmount=" + orderAmount +
                ", orderDate=" + orderDate +
                ", amountPayed=" + amountPayed +
                ", farmer=" + farmer +
                ", orderItems=" + orderItems +
                '}';
    }
}
