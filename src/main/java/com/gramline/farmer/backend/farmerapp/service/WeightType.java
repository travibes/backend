package com.gramline.farmer.backend.farmerapp.service;

public enum WeightType {
    LTR, KG;
}
