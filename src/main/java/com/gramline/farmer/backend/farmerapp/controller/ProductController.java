package com.gramline.farmer.backend.farmerapp.controller;

import com.gramline.farmer.backend.farmerapp.model.Product;
import com.gramline.farmer.backend.farmerapp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/saveAllProducts", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity saveProducts(@RequestBody List<Product> products) {
        productService.saveProducts(products);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(value = "/getProducts", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Product>> getProducts(@RequestParam("pageNumber") int pageNumber, @RequestParam("pageSize") int pageSize) {
        return ResponseEntity.ok(productService.findProductPaging(pageNumber, pageSize));
    }
}
