package com.gramline.farmer.backend.farmerapp.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class FarmData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Float area;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getArea() {
        return area;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "FarmData{" +
                "id=" + id +
                ", area=" + area +
                '}';
    }
}
