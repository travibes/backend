package com.gramline.farmer.backend.farmerapp.service;

import com.gramline.farmer.backend.farmerapp.model.Order;
import com.gramline.farmer.backend.farmerapp.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class OrderForDashboardService {

    @Autowired
    private OrderRepository orderRepository;

    public List<Order> getOrdersForDashboard() {
        LocalDateTime startDate = LocalDateTime.now();
        LocalDateTime endDate = startDate.minusMonths(3);
        List<Order> getAllOrdersBetween = orderRepository.findByOrderDateBetween(startDate, endDate);
        return getAllOrdersBetween;
    }
}
