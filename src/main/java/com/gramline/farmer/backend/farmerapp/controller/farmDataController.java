package com.gramline.farmer.backend.farmerapp.controller;

import com.gramline.farmer.backend.farmerapp.model.FarmData;
import com.gramline.farmer.backend.farmerapp.repository.FarmDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class farmDataController {

    @Autowired
    private FarmDataRepository farmDataRepository;

    @RequestMapping(value = "/savefarmdata", method = RequestMethod.GET)
    @ResponseBody
    public int getData(@RequestParam("id") int id) {
        return id;
    }

    @RequestMapping(value = "/setfarmdata", method = RequestMethod.POST)
    @ResponseBody
    public String saveNewFarmData(@RequestBody FarmData farmData) {
        farmDataRepository.save(farmData);
        return "Saved";
    }

}
