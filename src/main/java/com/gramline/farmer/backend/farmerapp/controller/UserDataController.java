package com.gramline.farmer.backend.farmerapp.controller;

import com.gramline.farmer.backend.farmerapp.model.Farmer;
import com.gramline.farmer.backend.farmerapp.repository.FarmerRepository;
import com.gramline.farmer.backend.farmerapp.service.FarmerDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserDataController {

    @Autowired
    private FarmerDetailService farmerDetailService;

    @RequestMapping(value = "/saveuserdetails", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity saveFpoDetails(@RequestBody Farmer farmerPrimaryDetails) {
        return ResponseEntity.ok(farmerDetailService.savedetails(farmerPrimaryDetails));
    }
}
