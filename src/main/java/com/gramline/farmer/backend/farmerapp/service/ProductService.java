package com.gramline.farmer.backend.farmerapp.service;

import com.gramline.farmer.backend.farmerapp.model.Product;
import com.gramline.farmer.backend.farmerapp.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    // Save product lists
    public void saveProducts(List<Product> products) {
        productRepository.saveAll(products);
    }

    // Save product
    public void saveProduct(Product product) {
        productRepository.save(product);
    }

    //Get products using pagination/range
    public List<Product> findProductPaging(int pageNumber, int pageSize) {
        return productRepository.findAll(PageRequest.of(pageNumber, pageSize)).getContent();
    }
}
