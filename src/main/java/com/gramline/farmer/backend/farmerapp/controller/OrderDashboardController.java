package com.gramline.farmer.backend.farmerapp.controller;

import com.gramline.farmer.backend.farmerapp.model.Order;
import com.gramline.farmer.backend.farmerapp.service.OrderForDashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderDashboardController {

    @Autowired
    private OrderForDashboardService orderForDashboardService;

    @RequestMapping(value = "/getAllProducts", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Order>> getOrders() {
        //orderForDashboardService.getOrdersForDashboard();
        return ResponseEntity.ok(orderForDashboardService.getOrdersForDashboard());
    }
}