package com.gramline.farmer.backend.farmerapp.repository;

import com.gramline.farmer.backend.farmerapp.model.Crop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface CropRepository extends CrudRepository<Crop, Integer> {
}
