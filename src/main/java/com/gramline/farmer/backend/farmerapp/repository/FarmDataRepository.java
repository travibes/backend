package com.gramline.farmer.backend.farmerapp.repository;

import com.gramline.farmer.backend.farmerapp.model.FarmData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface FarmDataRepository extends CrudRepository<FarmData, Integer> {
}
