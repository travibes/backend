package com.gramline.farmer.backend.farmerapp.model;

import com.gramline.farmer.backend.farmerapp.service.AddressType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AddressType addressType;

    @Column(nullable=false)
    private String fullAddress;

//    @Column(nullable=false)
//    private String village;
//
//    @Column(nullable=false)
//    private String tehsil;
//
//    @Column(nullable = false)
//    private String district;

    @Column(nullable = false)
    private String state;

//    @Column(nullable=false)
//    private String country;

    @Column(nullable=false, length = 10)
    private String pincode;

    @OneToOne(mappedBy = "address")
    private Farmer farmer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", addressType=" + addressType +
                ", fullAddress='" + fullAddress + '\'' +
                ", state='" + state + '\'' +
                ", pincode='" + pincode + '\'' +
                ", farmer=" + farmer +
                '}';
    }
}
