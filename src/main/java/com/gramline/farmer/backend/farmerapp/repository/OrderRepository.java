package com.gramline.farmer.backend.farmerapp.repository;

import com.gramline.farmer.backend.farmerapp.model.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;
import java.util.List;

@Component
public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByOrderDateBetween(LocalDateTime startDate, LocalDateTime endDate);
}
