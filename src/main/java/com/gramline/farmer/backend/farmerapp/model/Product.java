package com.gramline.farmer.backend.farmerapp.model;

import com.gramline.farmer.backend.farmerapp.service.ProductType;
import com.gramline.farmer.backend.farmerapp.service.WeightType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ProductType productType;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private String brand;

    @Column(nullable = false)
    private String variety;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private WeightType weightType;

    @Column(nullable = false)
    private Double productWeight;

    @Column(nullable = false)
    private String imageUrl;

    //@OneToOne(mappedBy = "product")
    //private OrderItem orderItem;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public WeightType getWeightType() {
        return weightType;
    }

    public void setWeightType(WeightType weightType) {
        this.weightType = weightType;
    }

    public Double getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(Double productWeight) {
        this.productWeight = productWeight;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", productType=" + productType +
                ", price=" + price +
                ", brand='" + brand + '\'' +
                ", variety='" + variety + '\'' +
                ", weightType=" + weightType +
                ", productWeight=" + productWeight +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
